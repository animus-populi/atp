'use strict'

/* eslint-env mocha */

const ATP = require('..')
const atp = ATP()

describe('simple bundeling', () => {
  let task

  it('should be able to define a task', () => {
    task = atp.defineTask({
      // maybe namespace can be deterministic/non-deterministic flag instead? (for cache)
      namespace: ATP.Namespace.Transformative, // this can be creative (for creation  of stuff), transformative (for non-deterministic things like rendering video files) or phyisical for physical labor, or trade for thing trades?
      tool: '/development/create-website/fs+bundle'
    }, {
      humanReadable: 'Website should include a simple hello-world in red, italic font',
      alsoBundle: true
    }, {
      limits: {
        // notBefore, notAfter, maxCredits, maxCreditsPerSubTask, etc
        maxCredits: 1000
      }
    })
  })

  it('should be able to define a tool', () => {
    atp.loadTool({
      namespace: ATP.Namespace.Transformative,
      tool: '/development/create-website/fs+bundle',
      permissions: []
    }, async (input, $atp) => {
      const [website] = await $atp.Task([{
        target: {
          namespace: ATP.Namespace.Creative,
          tool: 'create-website'
        },
        work: input.humanReadable + '. Output must be in FS format!'
      }])

      let bundeled

      if (input.alsoBundle) {
        bundeled = await $atp.Task({
          target: {
            namespace: ATP.Namespace.Transformative,
            tool: '/bundle/parcel-on-atp/v2' // this can be anything, really, but let's use this because paths are very good for versioning
          },
          work: {
            fs: website.fs
          }
        })
        bundeled = bundeled[0].fs
      }

      return ATP.Result({
        website: website.fs,
        bundeled
      })
    })

    atp.loadTool({
      namespace: ATP.Namespace.Creative,
      tool: 'create-website',
      permissions: []
    }, async (input) => {
      // super-complex ai

      if (input === 'Website should include a simple hello-world in red, italic font. Output must be in FS format!') {
        return ATP.Result({
          fs: {
            data: `<!doctype html>\n<html>\n<head>\n  <title>Hello World</title>\n</head>\n<body>\n  <h2 style="color: red"></h2>\n</body>\n</html>`,
            path: 'index.html'
          }
        }, {
          cost: 100
        })
      } else {
        throw new ATP.Error.UnableToFullfill('Not a real AI')
      }
    })
  })

  it('should be able to execute a task', async () => {
    const website = await ATP.Task(task)
    console.log(website)
  })
})
