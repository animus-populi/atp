'use strict'

const protons = require('protons')

module.exports = `

message Target {
  bytes predefinedTarget = 1; // this is the predefined target, for example a specific vendor. if not given,
}

message Work {

}

message Task { // TODO: make it so that a standalone task can be hashed to a CID without any owner-etc tags
  bytes owner = 1; // peerId of the owner
  Target targetParameters = 2;
  Work workParameters = 3;
}

`
